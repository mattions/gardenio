# Gardenio: keep track of your plants

Project to keep track of the water in your plan and then 
water them in case.

Package Organization:

	- gardenio_msgs: gardenio custom messages
	- gardenio_description: Gardenio description


# Launch

Launch roscore

	roscore
	
Launch the listener

	rosrun gardenio_control soil_sensor_acquisition.py

Launch Arduino

	rosrun rosserial_python serial_node.py /dev/ttyACM0
	
## Using RosLaunch

	roslaunch gardenio_control humidity_sensor.launch
	
## Publishing

	rostopic pub /acquire_soil_humidity std_msgs/Empty --once
	

# Building with RosSerial

	catkin_make gardenio_control_firmware_gardenio_control
	catkin_make gardenio_control_firmware_gardenio_control-upload
	

# Note on building custom messages.

There is a bug (check this answers: https://answers.ros.org/question/270980/set-build-order-in-catkin-generate-custom-messages-before-generating-ros_lib/)
and this issue: https://github.com/ros-drivers/rosserial/issues/239.
If the custom messages contained in gardenio_msgs are changed, then we need to:

Build everything (`gardenio_control` will fail. It's ok for now (Note, the `gardenio_msgs` package must be in the catking workspace)

	catkin_make
	
Remove ros_lib under `build/gardenio/gardenio_control`

	rm build/gardenio/gardenio_control -r
	
Build everything again, will force the message generation of roslib, and this time `gardenio_msgs` will be added.

	catkin_make
	
If the messages do not change, you can build as normal (`catkin_make`). If you are changing the messages definition
you need to do the same as before.


## Resolving everything from scratch

You may need to run
    
    rosdep install --from-paths src --ignore-src -r
    
from your root catkin folder before launching catkin


