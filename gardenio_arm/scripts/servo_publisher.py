#!/usr/bin/env python
from time import sleep
import rospy
from std_msgs.msg import UInt16

pub = rospy.Publisher('servo', UInt16, queue_size=10)
rospy.init_node('servo_angle_publisher')
r = rospy.Rate(10) # 10hz
while not rospy.is_shutdown():
    for angle in [0, 25, 150, 180]:
        rospy.loginfo("Setting the Servo to {0}".format(angle))
        pub.publish(angle)
        sleep(5)
    
    
   