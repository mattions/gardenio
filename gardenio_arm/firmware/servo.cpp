#include <Servo.h>
#include <ros.h>

#include <Arduino.h>

#include <std_msgs/UInt16.h>

ros::NodeHandle  nh;

Servo servo;

int const PUMP_PIN = 9;
int const SERVO_PIN = 10;
int const LED_PIN = 7;


// Message callback for the servo
void servo_cb( const std_msgs::UInt16& cmd_msg){
	pinMode(SERVO_PIN, OUTPUT); // powering the pin of the servo
	servo.write(cmd_msg.data); //set servo angle, should be from 0-180
	digitalWrite(13, HIGH-digitalRead(13));  //toggle led
	delay(1000);
	pinMode(SERVO_PIN, INPUT); // switching the servo off
}
ros::Subscriber<std_msgs::UInt16> servo_sub("servo", servo_cb);

void pump_cb( const std_msgs::UInt16& cmd_msg){
	nh.loginfo("Switching pump and servo ON!!");
	pinMode(SERVO_PIN, OUTPUT); // powering the pin of the servo
	digitalWrite(PUMP_PIN, HIGH);
	delay(cmd_msg.data);
	pinMode(PUMP_PIN, LOW);
	pinMode(SERVO_PIN, INPUT); // switching the servo off
	nh.loginfo("Switching pump and servo OFF!!");
}
ros::Subscriber<std_msgs::UInt16> pump_sub("pump", pump_cb);

void led_cb( const std_msgs::UInt16& led_duration){
	nh.loginfo("Blinking");
	// blink on and off for 5 seconds
	for (int i=0; i < 5; i++){
		digitalWrite(LED_PIN, HIGH);
		delay(500);
		digitalWrite(LED_PIN, LOW);
		delay(500);
	}
	nh.loginfo("Switch led ON for a duration");
	digitalWrite(LED_PIN, HIGH);
	delay(led_duration.data);
	digitalWrite(LED_PIN, LOW);
}
ros::Subscriber<std_msgs::UInt16> led_sub("led", led_cb);

void setup(){
  pinMode(13, OUTPUT);

  pinMode(LED_PIN, OUTPUT);
  //pinMode(SERVO_PIN, OUTPUT);
  servo.attach(SERVO_PIN);
  pinMode(PUMP_PIN, OUTPUT);

  nh.initNode();
  nh.subscribe(servo_sub);
  nh.subscribe(pump_sub);
  nh.subscribe(led_sub);
}

void loop(){
  nh.spinOnce();
  delay(1);

//  potVal = analogRead(potPin);
//  Serial.print("potVal: ");
//  Serial.print(potVal);
//  angle = map(potVal, 0, 1023, 0, 179);
//  Serial.print(" , angle: ");
//  Serial.println(angle);

//  servo.write(angle);

}
